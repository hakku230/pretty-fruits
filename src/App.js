import React from 'react';
import logo from './logo.svg';
import './App.css';
import './AppMobile.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Homepage from './components/Homepage'
import Single from './components/Single'

function App() {
  return (
    <Router>
        <header className="header">
          <div className="wrapper">
            <img className="header-logo" src={require("./assets/img/favicon-minified.png")} />
            <h1><Link to="/">Pretty Fruits</Link></h1>
            <input type="text" name="q" placeholder="Search something..." />
          </div>
        </header>

        <Switch>
          <Route path="/single/:id">
            <Single />
          </Route>
          <Route path="/">
            <Homepage />
          </Route>
        </Switch>

        <footer className="footer">
          <a href="https://gitlab.com/hakku230">©hakkyson</a>
        </footer>
    </Router>
  )
}

export default App;