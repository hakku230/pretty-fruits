import React from 'react';

import data from "../assets/data.json"

let Single = () => {
    let id = window.location.pathname.split('/')[2];
    let product = data.find( (value) => {if ( value.id == id ) return value;});
    product.gallery = [
      'strawberry.jpg',
      'strawberry.jpg',
      'strawberry.jpg',
      'strawberry.jpg',
    ]
    let seller = {
      name: 'John Doe',
      phone: '+7 (800) 555 35 35',
      avatar: require('../assets/img/ava.jpg')
    }
  
    return (
    <section className="single-product">
      <div className="wrapper">
        <div className="product-data">
          <div className="product-data__column product-data__column_left">
            <h1 className="product-data__title">{product.name}</h1>
            <img className="product-data__image" src={require(`../assets/img/products/${product.img}`)} />
            <div className="product-data__product-gallery">
              {
                product.gallery.map((value) => {
                  return (<img className="product-gallery__image" src={require(`../assets/img/products/${value}`)} />)
                })
              }
            </div>
          </div>
          <div className="product-data__column product-data__column_right">
            <div className="product-data__price">{product.cost}$</div>
            <div className="product-data__seller-container">
              <div className="product-data__seller-container-2">
                <a className="product-data__seller-name">{seller.name}</a>
                <img className="product-data__seller-avatar" src={seller.avatar}></img>
              </div>
              <a className="product-data__seller-phone" href={ "tel:" + seller.phone }>{seller.phone}</a>
              <button className="product-data__seller-message">Send a message</button>
            </div>
          </div>
        </div>
      </div>
    </section>
    )
}

export default Single