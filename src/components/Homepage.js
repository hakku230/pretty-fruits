import React from 'react';
import ProductCard from './ProductCard'

import data from "../assets/data.json"

let Homepage = () => {
    return (
    <section className="products">
      <div className="wrapper">
        {
          data.map((value, key) => {
            return <ProductCard value={value} key={key} />
          })
        }
      </div>  
    </section>
    )
}

export default Homepage