import React from 'react';
import { Link } from "react-router-dom";

let ProductCard = ( params ) => {
    let value = params.value;
  
    return (
      <Link to={'single/' + value.id + '/'} className="product-card" key={value.id}>
        <img className="product-card__image" src={require(`../assets/img/products/${value.img}`)} />
        <div className="product-card__info">
          <h3>{value.name}</h3>
          <p>{value.cost}$</p>
        </div>
      </Link>
    )
}

export default ProductCard